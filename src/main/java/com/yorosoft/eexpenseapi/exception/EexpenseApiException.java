package com.yorosoft.eexpenseapi.exception;

public class EexpenseApiException extends RuntimeException{
    public EexpenseApiException(String message) { super(message); }
}
