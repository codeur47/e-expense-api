package com.yorosoft.eexpenseapi.config;

public class CategoryControllerAPIPaths {
    public static final String BASE_URL = "/api/category";
    public static final String DELETE_BY_ID = "/{categoryId}";
    public static final String FIND_BY_ID = "/{categoryId}";

    private CategoryControllerAPIPaths() {
    }
}
