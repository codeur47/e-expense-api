package com.yorosoft.eexpenseapi.config;

public class AuthControllerAPIPaths {
    public static final String BASE_URL = "/api/auth";
    public static final String SIGNUP = "/signup";
    public static final String LOGIN = "/login";
    public static final String REFRESH_TOKEN = "/refresh/token";
    public static final String LOGOUT = "/logout";

    private AuthControllerAPIPaths() {
    }
}
