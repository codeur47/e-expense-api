package com.yorosoft.eexpenseapi.config;

public class PurchaseControllerAPIPaths {
    public static final String BASE_URL = "/api/purchase";
    public static final String DELETE_BY_ID = "/{purchaseId}";

    private PurchaseControllerAPIPaths() {
    }
}
