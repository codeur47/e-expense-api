package com.yorosoft.eexpenseapi.service;

import com.yorosoft.eexpenseapi.dto.CategoryRequest;
import com.yorosoft.eexpenseapi.exception.EexpenseApiException;
import com.yorosoft.eexpenseapi.exception.ResourceNotFoundException;
import com.yorosoft.eexpenseapi.model.Category;
import com.yorosoft.eexpenseapi.repository.CategoryRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@AllArgsConstructor
public class CategoryService {

    private final CategoryRepository categoryRepository;
    private final AuthService authService;

    public void create(CategoryRequest categoryRequest){
        categoryRepository.save(new Category(categoryRequest.getName(), Instant.now(), authService.getCurrentUser()));
    }

    public List<Category> categoriesByUser(){
        return categoryRepository.findAllByUser(authService.getCurrentUser());
    }

    public Optional<Category> findById(Long id){
        return categoryRepository.findById(id);
    }

    public Map<String, Boolean> delete(Long id) throws ResourceNotFoundException {
        Category category = findById(id)
                .orElseThrow(()->new ResourceNotFoundException("Category not found for this ID :" +id));

        categoryRepository.delete(category);
        Map<String, Boolean> response = new HashMap<>();
        response.put("category deleted", Boolean.TRUE);

        return response;
    }

    public Map<String, Boolean> update(CategoryRequest categoryRequest) throws ResourceNotFoundException {
        Category category = findById(categoryRequest.getCategoryId())
                .orElseThrow(()->new ResourceNotFoundException("Category not found for this ID :" +categoryRequest.getCategoryId()));

        category.setName(categoryRequest.getName());
        categoryRepository.save(category);

        Map<String, Boolean> response = new HashMap<>();
        response.put("category updated", Boolean.TRUE);

        return response;
    }
}
