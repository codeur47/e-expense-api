package com.yorosoft.eexpenseapi.service;


import com.yorosoft.eexpenseapi.dto.PurchaseRequest;
import com.yorosoft.eexpenseapi.exception.EexpenseApiException;
import com.yorosoft.eexpenseapi.exception.ResourceNotFoundException;
import com.yorosoft.eexpenseapi.model.Purchase;
import com.yorosoft.eexpenseapi.model.User;
import com.yorosoft.eexpenseapi.repository.PurchaseRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
@AllArgsConstructor
public class PurchaseService {

    private final PurchaseRepository purchaseRepository;
    private final CategoryService categoryService;
    private final AuthService authService;

    public void create(PurchaseRequest purchaseRequest){
        var purchaseCreated = new Purchase();
        var category = categoryService.findById(purchaseRequest.getCategoryId())
                .orElseThrow(()->new EexpenseApiException("Category not found for this id : "
                        +purchaseRequest.getCategoryId()));

        purchaseCreated.setName(purchaseRequest.getName());
        purchaseCreated.setDescription(purchaseRequest.getDescription());
        purchaseCreated.setQuantity(purchaseRequest.getQuantity());
        purchaseCreated.setCreatedDate(Instant.now());
        purchaseCreated.setPrice(new BigDecimal(String.valueOf(purchaseRequest.getPrice())));
        purchaseCreated.setUser(authService.getCurrentUser());
        purchaseCreated.setCategory(category);
        purchaseCreated.setTotal(new BigDecimal(String.valueOf(purchaseRequest.getPrice())).multiply(new BigDecimal(purchaseRequest.getQuantity())));

        purchaseRepository.save(purchaseCreated);
    }

    public List<Purchase> purchasesByUser(){
        User currentUser = authService.getCurrentUser();
        return purchaseRepository.findAllByUser(currentUser);
    }

    public Optional<Purchase> findById(Long id){
        return purchaseRepository.findById(id);
    }

    public Map<String, Boolean> delete(Long id) throws ResourceNotFoundException {
        Purchase purchase = findById(id)
                .orElseThrow(()-> new ResourceNotFoundException("Purchase with not found with ID :"+id));

        purchaseRepository.delete(purchase);

        Map<String, Boolean> response = new HashMap<>();
        response.put("Purchase deleted", Boolean.TRUE);

        return response;
    }
}
