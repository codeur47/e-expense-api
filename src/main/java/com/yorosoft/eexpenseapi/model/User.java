package com.yorosoft.eexpenseapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.time.Instant;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long userId;

    @NotEmpty(message = "Username is required")
    @NotNull
    private String username;

    @NotEmpty(message = "Password is required")
    @NotNull
    private String password;

    @NotEmpty(message = "Lastname is required")
    @NotNull
    private String lastname;

    @NotEmpty(message = "Firstname is required")
    @NotNull
    private String firstname;

    private Instant created;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Purchase> purchases;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Category> categories;



}
