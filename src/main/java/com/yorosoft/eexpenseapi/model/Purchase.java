package com.yorosoft.eexpenseapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.math.BigDecimal;
import java.time.Instant;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Purchase {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long purchaseId;

    @NotEmpty(message = "Purchase name is required")
    @NotNull
    private String name;

    @NotEmpty(message = "Purchase description is required")
    @NotNull
    private String description;

    @NotEmpty(message = "Purchase price is required")
    @NotNull
    private BigDecimal price;

    @NotEmpty(message = "Purchase quantity is required")
    @NotNull
    private int quantity;

    private BigDecimal total;

    @NotEmpty(message = "Purchase created date is required")
    @NotNull
    private Instant createdDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoryid", referencedColumnName = "categoryid")
    private Category category;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    private User user;

    public Purchase(String name, String description, BigDecimal price, int quantity, BigDecimal total, Instant createdDate, Category category, User user) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.quantity = quantity;
        this.total = total;
        this.createdDate = createdDate;
        this.category = category;
        this.user = user;
    }
}
