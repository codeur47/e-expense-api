package com.yorosoft.eexpenseapi.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import java.time.Instant;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Category {

    @Id
    @GeneratedValue(strategy = IDENTITY)
    private Long categoryId;

    @NotEmpty(message = "Category name is required")
    @NotNull
    private String name;

    @NotEmpty(message = "Category created date is required")
    @NotNull
    private Instant createdDate;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Purchase> purchaseList;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userid", referencedColumnName = "userid")
    private User user;

    public Category(String name, Instant createdDate, User user) {
        this.name = name;
        this.createdDate = createdDate;
        this.user = user;
    }
}
