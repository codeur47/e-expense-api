package com.yorosoft.eexpenseapi;

import com.yorosoft.eexpenseapi.config.SwaggerConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;

@SpringBootApplication
@Import(SwaggerConfiguration.class)
public class EExpenseApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(EExpenseApiApplication.class, args);
	}

}
