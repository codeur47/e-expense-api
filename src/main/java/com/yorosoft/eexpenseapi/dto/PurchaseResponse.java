package com.yorosoft.eexpenseapi.dto;

import com.yorosoft.eexpenseapi.model.Category;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Instant;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseResponse {
    private Long purchaseId;
    private String name;
    private String description;
    private BigDecimal price;
    private int quantity;
    private BigDecimal total;
    private Instant createdDate;
    private CategoryResponse category;
}
