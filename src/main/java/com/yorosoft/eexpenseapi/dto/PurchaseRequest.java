package com.yorosoft.eexpenseapi.dto;
import com.yorosoft.eexpenseapi.model.Category;
import com.yorosoft.eexpenseapi.model.User;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PurchaseRequest {
    private String name;
    private String description;
    private BigDecimal price;
    private int quantity;
    private Long categoryId;
    private String username;
}
