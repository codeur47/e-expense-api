package com.yorosoft.eexpenseapi.repository;

import com.yorosoft.eexpenseapi.model.Category;
import com.yorosoft.eexpenseapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category, Long> {
    List<Category> findAllByUser(User user);
}
