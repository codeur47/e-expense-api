package com.yorosoft.eexpenseapi.repository;

import com.yorosoft.eexpenseapi.model.Purchase;
import com.yorosoft.eexpenseapi.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PurchaseRepository extends JpaRepository<Purchase, Long> {
    List<Purchase> findAllByUser(User user);
}
