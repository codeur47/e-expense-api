package com.yorosoft.eexpenseapi.controller;

import com.yorosoft.eexpenseapi.config.CategoryControllerAPIPaths;
import com.yorosoft.eexpenseapi.dto.CategoryRequest;
import com.yorosoft.eexpenseapi.dto.CategoryResponse;
import com.yorosoft.eexpenseapi.exception.ResourceNotFoundException;
import com.yorosoft.eexpenseapi.mapper.EexpenseMapper;
import com.yorosoft.eexpenseapi.service.CategoryService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(CategoryControllerAPIPaths.BASE_URL)
@AllArgsConstructor
public class CategoryController {

    private final CategoryService categoryService;
    private final EexpenseMapper eexpenseMapper;

    @PostMapping
    public ResponseEntity<String> create(@RequestBody CategoryRequest categoryRequest){
        categoryService.create(categoryRequest);
        return new ResponseEntity<>("Category created", HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<CategoryResponse>> categoriesByUser(){
        return new ResponseEntity<>(eexpenseMapper.mapCategoryEntitiesToResponseDtos(categoryService.categoriesByUser()), HttpStatus.OK);
    }

    @DeleteMapping(CategoryControllerAPIPaths.DELETE_BY_ID)
    public ResponseEntity<Map<String, Boolean>> delete(@PathVariable(value = "categoryId") Long categoryId) throws ResourceNotFoundException {
        return new ResponseEntity<>(categoryService.delete(categoryId), HttpStatus.OK);
    }

    @GetMapping(CategoryControllerAPIPaths.FIND_BY_ID)
    public ResponseEntity<CategoryResponse> findCategoryById(@PathVariable(value = "categoryId") Long categoryId) throws ResourceNotFoundException {
        return new ResponseEntity<>(eexpenseMapper.mapCategoryEntityToResponseDto(categoryService.findById(categoryId).orElseThrow(()->new ResourceNotFoundException("Category not found for this ID :" +categoryId))), HttpStatus.OK);
    }

    @PutMapping()
    public ResponseEntity<Map<String, Boolean>> update(@RequestBody CategoryRequest categoryRequest) throws ResourceNotFoundException {
        return new ResponseEntity<>(categoryService.update(categoryRequest), HttpStatus.OK);
    }

}
