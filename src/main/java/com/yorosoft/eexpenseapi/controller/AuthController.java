package com.yorosoft.eexpenseapi.controller;

import com.yorosoft.eexpenseapi.config.AuthControllerAPIPaths;
import com.yorosoft.eexpenseapi.dto.AuthenticationResponse;
import com.yorosoft.eexpenseapi.dto.LoginRequest;
import com.yorosoft.eexpenseapi.dto.RefreshTokenRequest;
import com.yorosoft.eexpenseapi.dto.RegisterRequest;
import com.yorosoft.eexpenseapi.service.AuthService;
import com.yorosoft.eexpenseapi.service.RefreshTokenService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

import static org.springframework.http.HttpStatus.OK;

@RestController
@RequestMapping(AuthControllerAPIPaths.BASE_URL)
@AllArgsConstructor
public class AuthController {

    private final AuthService authService;
    private final RefreshTokenService refreshTokenService;

    @PostMapping(AuthControllerAPIPaths.SIGNUP)
    public ResponseEntity<String> signup(@RequestBody RegisterRequest registerRequest) {
        authService.signup(registerRequest);
        return new ResponseEntity<>("User Registration Successful", OK);
    }

    @PostMapping(AuthControllerAPIPaths.LOGIN)
    public ResponseEntity<AuthenticationResponse> login(@RequestBody LoginRequest loginRequest) {
        return new ResponseEntity<>(authService.login(loginRequest), OK);
    }

    @PostMapping(AuthControllerAPIPaths.REFRESH_TOKEN)
    public ResponseEntity<AuthenticationResponse> refreshTokens(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        return new ResponseEntity<>(authService.refreshToken(refreshTokenRequest), OK);
    }

    @PostMapping(AuthControllerAPIPaths.LOGOUT)
    public ResponseEntity<String> logout(@Valid @RequestBody RefreshTokenRequest refreshTokenRequest) {
        refreshTokenService.deleteRefreshToken(refreshTokenRequest.getRefreshToken());
        return ResponseEntity.status(OK).body("Refresh Token Deleted Successfully!!");
    }
}
