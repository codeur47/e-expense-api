package com.yorosoft.eexpenseapi.controller;

import com.yorosoft.eexpenseapi.config.PurchaseControllerAPIPaths;
import com.yorosoft.eexpenseapi.dto.PurchaseRequest;
import com.yorosoft.eexpenseapi.dto.PurchaseResponse;
import com.yorosoft.eexpenseapi.exception.ResourceNotFoundException;
import com.yorosoft.eexpenseapi.mapper.EexpenseMapper;
import com.yorosoft.eexpenseapi.service.PurchaseService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(PurchaseControllerAPIPaths.BASE_URL)
@AllArgsConstructor
public class PurchaseController {

    private final PurchaseService purchaseService;
    private final EexpenseMapper eexpenseMapper;

    @PostMapping
    public ResponseEntity<String> create(@RequestBody PurchaseRequest purchaseRequest){
        purchaseService.create(purchaseRequest);
        return new ResponseEntity<>("Purchase created", HttpStatus.OK);
    }

    @GetMapping()
    public ResponseEntity<List<PurchaseResponse>> purchasesByUser(){
        return new ResponseEntity<>(eexpenseMapper.mapPurchasesEntityToDtoResponses(purchaseService.purchasesByUser()), HttpStatus.OK);
    }

    @DeleteMapping(PurchaseControllerAPIPaths.DELETE_BY_ID)
    public ResponseEntity<Map<String, Boolean>> delete(@PathVariable(value = "purchaseId") Long purchaseId) throws ResourceNotFoundException {
        return new ResponseEntity<>(purchaseService.delete(purchaseId), HttpStatus.OK);
    }
}
