package com.yorosoft.eexpenseapi.mapper;

import com.yorosoft.eexpenseapi.dto.CategoryRequest;
import com.yorosoft.eexpenseapi.dto.CategoryResponse;
import com.yorosoft.eexpenseapi.dto.PurchaseRequest;
import com.yorosoft.eexpenseapi.dto.PurchaseResponse;
import com.yorosoft.eexpenseapi.model.Category;
import com.yorosoft.eexpenseapi.model.Purchase;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface EexpenseMapper {

    CategoryResponse mapCategoryEntityToResponseDto(Category category);

    List<CategoryResponse> mapCategoryEntitiesToResponseDtos(List<Category> categories);

    Category mapCategoryDtoRequestToEntity(CategoryRequest categoryRequest);

    Category mapCategoryDtoResponseToEntity(CategoryResponse categoryResponse);

    PurchaseResponse mapPurchaseEntityToDtoResponse(Purchase purchase);

    PurchaseRequest mapPurchaseEntityToDtoRequest(Purchase purchase);

    Purchase mapPurchaseRequestToEntity(PurchaseRequest purchaseRequest);

    Purchase mapPurchaseDtoResponseToEntity(PurchaseResponse purchaseResponse);

    List<PurchaseResponse> mapPurchasesEntityToDtoResponses(List<Purchase> purchases);

}
